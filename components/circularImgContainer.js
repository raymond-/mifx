import {View} from 'react-native';
import React from 'react';

export default function CircularImgContainer({children, style}) {
  return (
    <View
      style={[
        {
          justifyContent: 'center',
          alignItems: 'center',
          backgroundColor: 'white',
          width: 40,
          height: 40,
          borderRadius: 50,
        },
        style,
      ]}>
      {children}
    </View>
  );
}
