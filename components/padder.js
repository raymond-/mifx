import {View} from 'react-native';
import React from 'react';

export default function Padder({children, style}) {
  return (
    <View style={[{paddingHorizontal: 8, paddingVertical: 16}, style]}>
      {children}
    </View>
  );
}
