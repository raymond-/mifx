import {
  View,
  Text,
  FlatList,
  SafeAreaView,
  Image,
  StatusBar,
} from 'react-native';
import React, {useEffect, useState} from 'react';
import {api} from './apicall';
import Feather from 'react-native-vector-icons/Feather';
import FontAwesomeIcon from 'react-native-vector-icons/FontAwesome';
import CircularImgContainer from './components/circularImgContainer';
import Padder from './components/padder';

export default function App() {
  const [productData, setProductData] = useState();
  const [productCategory, setProductCategory] = useState();

  useEffect(() => {
    api.get('/products').then(res => {
      setProductData(res?.data);
    });
    api.get('/category').then(res => {
      setProductCategory(res?.data);
    });
  }, []);

  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: 'rgba(248, 248, 248, 1)',
      }}>
      <StatusBar
        barStyle="dark-content"
        backgroundColor="rgba(248, 248, 248, 1)"
      />
      <Padder
        style={{
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
        }}>
        <CircularImgContainer>
          <Feather name="arrow-left" color="black" size={30} />
        </CircularImgContainer>
        <Text style={{flex: 1, marginLeft: 16}}>Shoes</Text>
        <CircularImgContainer>
          <FontAwesomeIcon name="sliders" color="black" size={30} />
        </CircularImgContainer>
      </Padder>
      <View>
        <FlatList
          showsHorizontalScrollIndicator={false}
          contentContainerStyle={{paddingBottom: 16}}
          keyExtractor={item => {
            return item?.id;
          }}
          data={productCategory}
          horizontal
          renderItem={({item}) => {
            return (
              <View
                style={{
                  justifyContent: 'center',
                  alignItems: 'center',
                  width: 100,
                  height: 50,
                  backgroundColor: 'white',
                  marginRight: 12,
                }}>
                <Text>{item?.name}</Text>
              </View>
            );
          }}
        />
      </View>

      <FlatList
        contentContainerStyle={{
          justifyContent: 'center',
          alignItems: 'center',
          paddingBottom: 100,
        }}
        showsVerticalScrollIndicator={false}
        numColumns={2}
        horizontal={false}
        keyExtractor={item => {
          return item?.id;
        }}
        data={productData}
        renderItem={({item}) => {
          const totalStars = 5;

          return (
            <View
              style={{
                justifyContent: 'center',
                alignItems: 'center',
                width: 180,
                height: 200,
                backgroundColor: 'white',
                marginRight: 10,
                marginTop: 10,
                borderRadius: 16,
                paddingTop: 16,
                shadowColor: 'rgba(163, 165, 168, 1)',
                shadowOffset: {
                  width: 0,
                  height: 1,
                },
                shadowOpacity: 0.22,
                shadowRadius: 2.22,
                elevation: 3,
              }}>
              <View style={{alignItems: 'center'}}>
                <View
                  style={{
                    backgroundColor: 'rgba(248, 248, 250, 1)',
                    width: 150,
                    position: 'relative',
                  }}>
                  <Image
                    source={{uri: item?.image}}
                    resizeMode="contain"
                    style={{height: 100}}
                  />
                  {item?.out_of_stock && (
                    <View
                      style={{
                        position: 'absolute',
                        backgroundColor: 'rgba(243, 110, 90, 1)',
                        paddingHorizontal: 8,
                        paddingVertical: 3,
                        borderRadius: 8,
                        top: -8,
                      }}>
                      <Text>Out of stock</Text>
                    </View>
                  )}
                  <CircularImgContainer
                    style={{position: 'absolute', right: -10}}>
                    <FontAwesomeIcon name="heart-o" color="grey" size={15} />
                  </CircularImgContainer>
                </View>
              </View>

              <View
                style={{
                  alignSelf: 'flex-start',
                  paddingLeft: 16,
                  marginTop: 16,
                }}>
                <View style={{flexDirection: 'row'}}>
                  {[...new Array(totalStars)].map((_, index) => {
                    return index < item?.rating ? (
                      <FontAwesomeIcon name="star" color="yellow" size={15} />
                    ) : (
                      <FontAwesomeIcon name="star-o" color="grey" size={15} />
                    );
                  })}
                </View>
                <Text>{item?.name}</Text>
              </View>
              <View
                style={{
                  paddingHorizontal: 16,
                  flex: 1,
                  flexDirection: 'row',
                  justifyContent: 'space-between',
                }}>
                <Text style={{flex: 1}}>{item?.price}</Text>
                <Text>{item?.off}</Text>
              </View>
            </View>
          );
        }}
      />
    </SafeAreaView>
  );
}
