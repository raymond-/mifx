import axios from 'axios';

export const api = axios.create({
  timeout: 10000,
  headers: {
    'X-Requested-With': 'XMLHttpRequest',
    'Cache-Control': 'no-store',
  },
  baseURL: 'https://fe.dev.dxtr.asia/api',
});

api.interceptors.request.use(
  request => {
    console.log('<<< request: ', request);
    return request;
  },
  error => {
    console.log('>>> error: ', error);
    Promise.reject(error);
  },
);

api.interceptors.response.use(
  response => {
    console.log('<<< Response: ', response);
    return response;
  },
  error => {
    console.log('>>> error: ', error);
  },
);
